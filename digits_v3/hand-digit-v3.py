
import numpy as np 
import matplotlib
import matplotlib.pyplot as plt
import joblib

from sklearn.datasets import fetch_mldata

mnist = fetch_mldata('mnist-original', data_home='./')
N,d = mnist.data.shape
print(N)
print(d)

x_all = mnist.data 
y_all = mnist.target

plt.imshow(x_all.T[:,3000].reshape(28,28))
plt.axis("off")
plt.show()

x0 = x_all[np.where(y_all == 0)[0]]
x1 = x_all[np.where(y_all == 1)[0]]
y0 = np.zeros(x0.shape[0])
y1 = np.ones(x1.shape[0])

X = np.concatenate((x0,x1), axis = 0)
y = np.concatenate((y0,y1))

one =np.ones((X.shape[0],1))
X = np.concatenate((X,one), axis=1)

def gradent_decent(X,y, theta_inist, eta=0.05):
    theta_old = theta_inist
    theta_epoch = theta_inist
    N = X.shape[0]
    for it in range(10000):
        mrx_id = np.random.permutation(N)
        for i in mrx_id:
            xi = X[i,:]
            yi = y[i]
            hi = 1.0/ (1+np.exp(-np.dot(xi, theta_old.T)))
            gi = (yi - hi)* xi
            theta_new = theta_old + eta*gi
            theta_old= theta_new

        if np.linalg.norm(theta_epoch-theta_old)< 1e-4:
            break

        theta_epoch = theta_old
    return theta_epoch, it

theta_init= np.random.randn(1, X.shape[1])[0]
theta, it= gradent_decent(X,y,theta_init)
np.savetxt('theta.txt', theta)
print ("nghiem:", theta)
print ("it:", it)


